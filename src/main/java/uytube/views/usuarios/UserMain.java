package uytube.views.usuarios;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import uytube.views.Frame;
import uytube.views.Inicio;
import uytube.views.usuarios.consultar.ConsultarMain;

import com.jgoodies.forms.layout.FormSpecs;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserMain extends JPanel {

	/**
	 * Create the panel.
	 */
	JFrame frame;
	public UserMain() {
		setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JButton btnNewButton = new JButton("Agregar usuario");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Alta alta = new Alta();
				Frame.frame.setContentPane(alta);
				Frame.frame.revalidate();
				
			}
		});
		
		JButton btnSeguirUsuario = new JButton("Seguir usuario");
		btnSeguirUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Seguir usuario panel");
				Seguir seguir = new Seguir();
				Frame.frame.setContentPane(seguir);
				Frame.frame.revalidate();	
				
			}
		});
		
		JButton btnDejarDeSeguir = new JButton("Dejar de seguir");
		btnDejarDeSeguir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DejarSeguir dejarSeguir = new DejarSeguir();
				Frame.frame.setContentPane(dejarSeguir);
				Frame.frame.revalidate();
			}
		});
		add(btnDejarDeSeguir, "4, 6");
		add(btnSeguirUsuario, "4, 8");
		add(btnNewButton, "4, 10");
		
		JButton btnNewButton_2 = new JButton("Consultar usuario");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConsultarMain Consultar = new ConsultarMain();
				Frame.frame.setContentPane(Consultar);
				Frame.frame.revalidate();				
				
			}
		});
		add(btnNewButton_2, "4, 12");
		
		JButton btnNewButton_1 = new JButton("Listar usuarios");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Listar listar = new Listar();
				Frame.frame.setContentPane(listar);
				Frame.frame.revalidate();				
				
			}
		});
		add(btnNewButton_1, "4, 14");
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio inicio = new Inicio();
				Frame.frame.setContentPane(inicio);
				Frame.frame.revalidate();				
			}
		});
		add(btnVolver, "4, 18");

	}

}
